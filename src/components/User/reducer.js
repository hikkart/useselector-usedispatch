export const sampleUser = (state = {}, action) => {
  switch (action.type) {
    case "SAMPLE_LOGIN":
      return {
        ...state,
        user: action.payload,
        loggedIn: true
      };
    case "SAMPLE_LOGOUT":
      return {
        ...state,
        user: {},
        loggedIn: false
      };
    default:
      return state;
  }
};

import React, { useEffect } from "react";
import actions from "./actions";
import { useSelector, useDispatch } from "react-redux";

function User() {
  const sampleUser = useSelector(state => state.sampleUser);

  const dispatch = useDispatch();

  const user = {
    name: "rtewar"
  };

  useEffect(() => {
    dispatch(actions.login(user));
  }, []);

  return (
    <div>
      {sampleUser.loggedIn ? (
        <>
          <h1>Finally::: {sampleUser.user.name}</h1>
          <button onClick={() => dispatch(actions.logout())}>Logout</button>
        </>
      ) : (
        <>
          <h1>Login</h1>
          <button onClick={() => dispatch(actions.login(user))}>Login</button>
        </>
      )}
    </div>
  );
}

export default User;

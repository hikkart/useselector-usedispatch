//Login Action
//Logout Action

const login = userObj => {
  return {
    type: "USER_LOGIN",
    payload: userObj
  };
};

const logout = () => {
  return {
    type: "USER_LOGOUT",
    payload: {}
  };
};

const actions = { login, logout };

export default actions;

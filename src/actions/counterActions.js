//Increment Action
//Decrement Action

const increment = () => {
  return {
    type: "COUNTER_INC"
  };
};

const decrement = () => {
  return {
    type: "COUNTER_DEC"
  };
};

export default { increment, decrement };

# WeatherApp

ReactJS UseEffect hook and React-Redux useSelector and useDispatch Sample Application.

## Getting Started

### Prerequisites

Please make sure "NodeJS" is installed your system. This application supports both Windows and Linux platforms.

```
Clone the project from the Gitlab:
```

## Deployment

Once you cloned and opened the "weatherapp" folder in Visual Studio Code. Open terminal using Ctrl + ` and execute the below commands.

```
npm install
```

```
npm start
```
## Built With

NodeJS & React-Redux, ReactJS

## Authors

- **Karthik K** - @[hikkart](https://gitlab.com/hikkart)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

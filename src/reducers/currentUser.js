export const currentUser = (state = {}, action) => {
  switch (action.type) {
    case "USER_LOGIN":
      return {
        ...state,
        user: action.payload,
        loggedIn: true
      };
    case "USER_LOGOUT":
      return {
        ...state,
        user: {},
        loggedIn: false
      };
    default:
      return state;
  }
};

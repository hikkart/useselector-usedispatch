import { combineReducers } from "redux";
import { currentCounter } from "./currentCounter";
import { currentUser } from "./currentUser";
import { sampleUser } from "../components/User/reducer";

const rootReducer = combineReducers({
  currentCounter,
  currentUser,
  sampleUser
});

export default rootReducer;

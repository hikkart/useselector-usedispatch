const login = userObj => {
  return {
    type: "SAMPLE_LOGIN",
    payload: userObj
  };
};

const logout = () => {
  return {
    type: "SAMPLE_LOGOUT",
    payload: {}
  };
};

const actions = { login, logout };

export default actions;

import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./App.css";
import allActions from "./actions";
import User from "./components/User";

function App() {
  const currentCounter = useSelector(state => state.currentCounter);
  const currentUser = useSelector(state => state.currentUser);

  const dispatch = useDispatch();

  const user = { name: "karthik" };

  useEffect(() => {
    dispatch(allActions.userActions.login(user));
  }, []);

  return (
    <div className="App">
      {currentUser.loggedIn ? (
        <>
          <h1>HI is {currentUser.user.name}</h1>
          <button onClick={() => dispatch(allActions.userActions.logout())}>
            Logout
          </button>
        </>
      ) : (
        <>
          <h1>Login</h1>
          <button onClick={() => dispatch(allActions.userActions.login(user))}>
            Login
          </button>
        </>
      )}
      <br />
      <h1>Counter: {currentCounter}</h1>
      <button onClick={() => dispatch(allActions.counterActions.increment())}>
        Increment
      </button>
      <button onClick={() => dispatch(allActions.counterActions.decrement())}>
        Decrement
      </button>
      <User />
    </div>
  );
}

export default App;
